<?php

use NetfilterRule;

class NetfilterInterface {

	public function createRule() {
		return new NetfilterRule;
	}

	public function acceptRule() {
		if ($this->rule != null) {
			exec("iptables ".$this->rule);
			return true;
		} else return false;
	}

}