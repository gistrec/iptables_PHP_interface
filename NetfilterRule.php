<?php

use NetfilterInterface;

class NetfilterRule extends NetfilterInterface {

	// Сюда запишется строка с правилом
	public $rule;

	/* Действие:
	 *  -A добавить правило
	 *  -D удалить правило
	 *  -R заменить правило
	 */
	public $action;

	/* Цепочка:
	 *  INPUT вошедший пакет
	 *  PREROUTING входящий пакет
	 */
	public $chain;

	/* Протокол:
	 *  UPD
	 *  TCP
	 */
	public $protocol;

	// Данные отправителя пакета
	public $sourceIp;
	public $sourcePort;

	// Порт назначения
	public $destinationPort;

	/* Действие:
	 *  DROP - уничтожение пакета
	 *  REDIRECT - перенаправление на ip/port
	 */
	public $newAction;

	// Redirect:
	public $redirectToPort;

	public function setAction($action) {
		switch (strtolower($action)) {
			case 'add': self::$action = "-A"; break;
			case 'delete': self::$action = "-D"; break;
			case 'replace': self::$action = "-R"; break;
		}
	}

	public function setChain($chain) {
		switch (strtolower($chain)) {
			case 'input': self::$chain = "INPUT"; break;
			case 'prerouting': self::$chain = "PREROUTING"; break;
		}
	}

	public function setProtocol($protocol) {
		switch (strtolower($protocol)) {
			case 'udp': self::$protocol = "udp";
			case 'tcp': self::$protocol = "tcp";
		}
	}

	public function setSourceIp($sourceIp) {
		self::$sourceIp = $sourceIp;
	}

	public function setSourcePort($sourcePort) {
		self::$sourcePort = $sourcePort;
	}

	public function setDistinationPort($destinationPort) {
		self::$distinationPort = $distinationPort;
	}

	public function setNewAction($newAction) {
		switch (strtolower($newAction)) {
			case 'drop': self::$newAction = "DROP";
			case 'redirect': self::$newAction = "REDIRECT";
		}
	}

	public function setNewPort($newPort) {
		self::$redirectToPort = $newPort;
	}

	public function createRule() {
		if (self::$action = NULL || self::$newAction == NULL || (self::$chain != "PREROUTING" && $self::$newAction == "REDIRECT") || ($self::$newAction == "REDIRECT" && self::$redirectToPort == NULL)) return false;
		$rule = "";
		$rule .= self::$action . " ";
		$rule .= self::$chain . " ";
		if (self::$protocol != NULL) $rule .= "-p " . self::$protocol . " ";
		if (self::$sourceIp != NULL) $rule .= "-s " . self::$sourceIp . " ";
		if (self::$sourcePort != NULL) $rule .= "--sport " . self::$sourcePort . " ";
		if (self::$distinationPort != NULL) $rule .= "--dport " . self::$distinationPort . " ";
		$rule .= "-j " . self::$newAction;
		if (self::$redirectToPort != NULL) $rule .= "--to-ports " . self::$redirectToPort;
		self::$rule = $rule;
		return true;
	}

}